# Lightweight Coordination Multi-Agent Planning (LCMAP)

A key issue in multi-agent planning is the agents’ coordination process that is a computacionally hard problem. Thus, many works focus on the planning strategy mainly considering the computational process, agents’
distribution roles, information privacy, and the resources coupling level. In this context, domain-independent models that explore the balance between coordination process and privacy leading to an efficient execution solution
are important. Thus, this project presents an efficient multi-agent planning model called LCMAP.
LCMAP is a domain-independent model that balances coordination process and privacy through three independent phases: (i) verification - each agent verifies its capabilities of reaching the goals by itself; (ii) transformation - the
coordinator selects agents through their capabilities and distributes the goals, transforming the original problem into single-agent problems; and (iii) validation - each individual plan is validated to check whether it can be carried
in parallel. LCMAP was compared to the state-of-the-art models to evaluate time efficiency and plan length during the problem solving process. We used loosely and tightly coupled domains with specific evaluation metrics inherited
from planning competions. Regarding real world-problems, LCMAP was evaluated as a solution for teamwork management presenting suitability for most of the phases: potential recognition, team formation, planning and team action. LCMAP presented a considerable performance gain of 100 times assuring scalability, what highlights the potential of the proposed model.

Obs: MAP concepts are presented [here](https://gitlab.com/InfoKnow/AutomatedPlanning/LeonardoMoreira-LCMAP/wikis/MAP-concepts). Experimental results are presented [here](https://gitlab.com/InfoKnow/AutomatedPlanning/LeonardoMoreira-LCMAP/wikis/uploads/37d169d75b8a66fb1abdf8277342eaa5/resultsPDF.pdf).
