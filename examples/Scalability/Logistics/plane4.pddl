(define 
	(problem logistics-4-0)
	(:domain logistics)
	(:objects
 		plane4 - plane
 		apt1 apt2 apt3 apt4 pos4 pos3 pos2 pos1 - location
 		cit4 cit3 cit2 cit1 - city
 		obj43 obj42 obj41 obj33 obj32 obj31 obj23 obj22 obj21 obj13 obj12 obj11 - package
	)

	(:init 

		(atPlane plane4 apt4)
		
		(atPackage obj43 pos4)
		(atPackage obj42 pos4)
		(atPackage obj41 pos4)
		(atPackage obj33 pos3)
		(atPackage obj32 pos3)
		(atPackage obj31 pos3)
		(atPackage obj23 pos2)
		(atPackage obj22 pos2)
		(atPackage obj21 pos2)
		(atPackage obj13 pos1)
		(atPackage obj12 pos1)
		(atPackage obj11 pos1)
		
		(inCity apt1 cit1)
		(inCity apt4 cit4)
		(inCity apt3 cit3)
		(inCity apt2 cit2)
		(inCity pos4 cit4)
		(inCity pos3 cit3)
		(inCity pos2 cit2)
		(inCity pos1 cit1)

		(isAirport apt1)
		(isAirport apt2)
		(isAirport apt3)
		(isAirport apt4)
		
	)
	(:goal 
		(and
			(atPackage obj31 pos3)
			(atPackage obj33 apt3)
			(atPackage obj41 apt3)
			(atPackage obj23 pos4)
			(atPackage obj11 pos3)
			(atPackage obj22 apt2)
			(atPackage obj12 apt1)
			(atPackage obj21 pos4)
			(atPackage obj42 pos4)
			(atPackage obj32 pos1)
		)
	)
)