(
  define (domain logisticTruck)
  (:requirements :typing)
(:types location city airport package truck)
  
  (:predicates 
    (atPackage ?p - package ?l - location)
    ;; (atPlane ?plane - plane ?l - location)
    ;; (inPlane ?p - package ?plane - plane)
    (inCity ?loc - location ?city - city)
    (isAirport ?loc - location)
    (atTruck ?truck - truck ?loc - location)
    (inTruck ?p - package ?truck - truck)
  )

(:action loadTruck
    :parameters (?p - package  ?loc - location ?truck - truck)
    :precondition 
    (and
      (atPackage ?p ?loc) (atTruck ?truck ?loc)
    )
    :effect
    (and
      (not(atPackage ?p ?loc)) (inTruck ?p ?truck)
    )
  )

  (:action unloadTruck
    :parameters (?p - package  ?loc - location ?truck - truck)
    :precondition
    (and
      (inTruck ?p ?truck) (atTruck ?truck ?loc)
    )
    :effect
    (and
      (not(inTruck ?p ?truck)) (atPackage ?p ?loc)
    )
  )

  (:action driveTruck
    :parameters (?truck - truck ?locFrom - location ?locTo - location ?city - city)
    :precondition
    (and
      (atTruck ?truck ?locFrom) (inCity ?locFrom ?city) (inCity ?locTo ?city)
    )
    :effect
    (and
      (not(atTruck ?truck ?locFrom)) (atTruck ?truck ?locTo)
    )
  )

)  