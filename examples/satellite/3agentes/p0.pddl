(define 
	(problem strips-sat-x-1)
	(:domain satellite)
	(:objects
		 satellite0 satellite1 satellite2 - satellite
		 instrument0 instrument1 instrument2 instrument3 instrument4 instrument5 instrument6 instrument7 instrument8 - instrument
		 thermograph0 image2 spectrograph1 - mode
		 groundstation2 groundstation1 groundstation0 star3 star4 phenomenon5 phenomenon6 star7 phenomenon8 planet9 - direction
	)
	
	(:init 
		(mySatellite satellite0)
		(power_avail satellite0)
		
		(calibration_target instrument0 groundstation2)
		
		(calibration_target instrument1 groundstation1)
		
		(calibration_target instrument2 groundstation0)
		
		(calibration_target instrument3 groundstation0)
		
		(calibration_target instrument4 groundstation2)
		
		(calibration_target instrument5 groundstation1)
		
		(calibration_target instrument6 groundstation1)
		
		(calibration_target instrument7 groundstation1)
		
		(calibration_target instrument8 groundstation0)
		
		(pointing satellite0 phenomenon8)
		(on_board satellite0 instrument0)
		(on_board satellite0 instrument1)
		(on_board satellite0 instrument2)
		
		(supports instrument0 thermograph0)
		(supports instrument0 image2)
		(supports instrument0 spectrograph1)
		(supports instrument1 thermograph0)
		(supports instrument1 image2)
		(supports instrument1 spectrograph1)
		(supports instrument2 image2)
		(supports instrument3 thermograph0)
		(supports instrument3 spectrograph1)
		(supports instrument4 image2)
		(supports instrument4 spectrograph1)
		(supports instrument5 thermograph0)
		(supports instrument5 image2)
		(supports instrument5 spectrograph1)
		(supports instrument6 image2)
		(supports instrument7 thermograph0)
		(supports instrument7 image2)
		(supports instrument8 thermograph0)
		(supports instrument8 image2)
		(supports instrument8 spectrograph1)
	)
	(:goal 
		(and
			 (pointing satellite0 phenomenon5)
			 (pointing satellite1 groundstation2)
			 (have_image star3 thermograph0)
			 (have_image phenomenon5 image2)
			 (have_image phenomenon6 image2)
			 (have_image star7 thermograph0)
			 (have_image phenomenon8 image2)
			 (have_image planet9 spectrograph1)
		)
	)
)