(define 
	(problem strips-sat-x-1)
	(:domain satellite)
	(:objects
	 satellite0 satellite1 satellite2 satellite3 - satellite
	 instrument0 instrument1 instrument2 instrument3 instrument4 instrument5 instrument6 instrument7 - instrument
	 image2 image1 image0 image3 - mode
	 star3 groundstation2 star1 groundstation4 groundstation0 phenomenon5 star6 star7 planet8 planet9 planet10 planet11 - direction
	)

	(:init 
		(mySatellite satellite1)
		(power_avail satellite1)
		
		(calibration_target instrument0 star1)
		
		(calibration_target instrument1 groundstation0)
		
		(calibration_target instrument2 groundstation2)
		
		(calibration_target instrument3 groundstation4)
		
		(calibration_target instrument4 star1)
		
		(calibration_target instrument5 star1)
		
		(calibration_target instrument6 groundstation4)
		
		(calibration_target instrument7 groundstation0)
		
		(pointing satellite1 groundstation0)
		(on_board satellite1 instrument3)
		
		(supports instrument0 image1)
		(supports instrument0 image3)
		(supports instrument1 image3)
		(supports instrument2 image0)
		(supports instrument3 image2)
		(supports instrument3 image0)
		(supports instrument4 image1)
		(supports instrument4 image0)
		(supports instrument5 image2)
		(supports instrument5 image1)
		(supports instrument5 image0)
		(supports instrument6 image2)
		(supports instrument6 image1)
		(supports instrument6 image0)
		(supports instrument7 image1)
		(supports instrument7 image3)
		(supports instrument7 image3)
	)
	(:goal 
		(and
		 (pointing satellite1 star1)
		 (pointing satellite2 phenomenon5)
		 (have_image phenomenon5 image0)
		 (have_image star6 image1)
		 (have_image star7 image0)
		 (have_image planet8 image0)
		 (have_image planet9 image3)
		 (have_image planet10 image0)
		 (have_image planet11 image2)
		)
	)
)