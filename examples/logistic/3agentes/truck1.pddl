(define 
	(problem logistics-4-0)
	(:domain logistics)
	(:objects
 		truck1 - truck
 		apt1 apt2 pos2 pos1 - location
 		cit2 cit1 - city
 		obj23 obj22 obj21 obj13 obj12 obj11 - package
	)

	(:init 
		(atTruck truck1 pos1)
		(atPackage obj23 pos2)
		(atPackage obj22 pos2)
		(atPackage obj21 pos2)
		(atPackage obj13 pos1)
		(atPackage obj12 pos1)
		(atPackage obj11 pos1)
		(inCity apt1 cit1)
		(inCity apt2 cit2)
		(inCity pos2 cit2)
		(inCity pos1 cit1)
		(isAirport apt1)
		(isAirport apt2)
	)
	(:goal 
		(and
	 		(atPackage obj11 apt1)
	 		(atPackage obj23 pos1)
	 		(atPackage obj13 apt1)
	 		(atPackage obj21 pos1)

		)
	)
)
