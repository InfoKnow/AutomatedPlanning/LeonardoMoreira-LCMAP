(
  define (domain logisticPlane)
  (:requirements :typing)
  (:types location city airport package plane)

  (:predicates 
    (atPackage ?p - package ?l - location)
    (atPlane ?plane - plane ?l - location)
    (inPlane ?p - package ?plane - plane)
    (inCity ?loc - location ?city - city)
    (isAirport ?loc - location)
  )

  (:action loadPlane
    :parameters (?p - package  ?loc - location ?plane - plane)
    :precondition 
    (and
      (atPackage ?p ?loc) (atPlane ?plane ?loc)
    )
    :effect
    (and
      (not(atPackage ?p ?loc)) (inPlane ?p ?plane)
    )
  )

  (:action unloadPlane
    :parameters (?p - package  ?loc - location ?plane - plane)
    :precondition
    (and
      (inPlane ?p ?plane) (atPlane ?plane ?loc)
    )
    :effect
    (and
      (not(inPlane ?p ?plane)) (atPackage ?p ?loc)
    )
  )

  (:action flyPlane
    :parameters (?plane - plane ?locFrom - location ?locTo - location)
    :precondition
    (and
      (atPlane ?plane ?locFrom) (isAirport ?locFrom) (isAirport ?locTo)
    )
    :effect
    (and
      (not(atPlane ?plane ?locFrom)) (atPlane ?plane ?locTo)
    )
  )

)  