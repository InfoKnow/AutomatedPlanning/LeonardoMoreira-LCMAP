(define 
  (domain depot)
  (:requirements :typing :equality :fluents)
  (:types place hoist surface depot distributor truck crate pallet)
  (:predicates
    (myAgent ?a - place)
    (clearC ?x - crate)
    (clearP ?x - pallet)
    (clearH ?x - hoist)
    (located ?h - hoist ?p - place)
    (at ?t - truck ?p - place)
    (placed ?p - pallet ?p - place)
    (posP ?c - crate ?p - place)
    (posT ?c - crate ?t - truck)
    (onP ?c - crate ?s - pallet)
    (onC ?c - crate ?s - crate)
    (onH ?c - crate ?h - hoist)
    (onT ?c - crate ?t - truck)
  )
 
  (:action LiftP
   :parameters (?h - hoist ?c - crate ?z - pallet ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      (placed ?z ?p) 
      (clearH ?h) 
      (posP ?c ?p)
      (onP ?c ?z) 
      (clearC ?c)
    )
   :effect 
    (and 
      ;;(assign (on ?c) ?h) 
      (onH ?c ?h) 
      (not (onP ?c ?z))
      (not (clearC ?c)) 
      (not (clearH ?h))
      (clearP ?z)
    )
  )
  
  (:action LiftC
   :parameters (?h - hoist ?c - crate ?z - crate ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      ;;(= (pos ?z) ?p) 
      (posP ?z ?p) 
      (clearH ?h)
      ;;(= (pos ?c) ?p) 
      (posP ?c ?p) 
      ;;(= (on ?c) ?z) 
      (onC ?c ?z) 
      (clearC ?c)
    )
   :effect 
    (and 
      ;;(assign (on ?c) ?h) 
      (onH ?c ?h)
      (not (onC ?c ?z))
      (not (clearC ?c)) 
      (not (clearH ?h))
      (clearC ?z)
    )
  )
  
  (:action DropP
   :parameters (?h - hoist ?c - crate ?z - pallet ?p - place)
   :precondition 
    (and (myAgent ?p) 
    (located ?h ?p)
    (placed ?z ?p) 
    (clearP ?z) 
    ;;(= (on ?c) ?h)
    (onH ?c ?h)
    (not (clearC ?c)) 
    (not (clearH ?h)))
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      (not (clearP ?z))
      ;;(assign (on ?c) ?z)
      (onP ?c ?z)
      (not (onH ?c ?h))
    )
  )
  
  (:action DropC
   :parameters (?h - hoist ?c - crate ?z - crate ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      (posP ?z ?p) 
      (clearC ?z)
      ;;(= (on ?c) ?h) 
      (onH ?c ?h) 
      (not (clearC ?c))
      (not (clearH ?h))
    )
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      (not (clearC ?z))
      ;;(assign (on ?c) ?z)
      (onC ?c ?z)
      (not (onH ?c ?h))
    )
  )
)