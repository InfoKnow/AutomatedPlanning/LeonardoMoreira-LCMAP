(define 
  (domain depot)
  (:requirements :typing :equality :fluents)
  (:types place hoist surface depot distributor truck crate pallet)
  (:predicates
    (myAgentTT ?a - truck)
    (clearC ?x - crate)
    (clearP ?x - pallet)
    (clearH ?x - hoist)
    (located ?h - hoist ?p - place)
    (at ?t - truck ?p - place)
    (placed ?p - pallet ?p - place)
    (posP ?c - crate ?p - place)
    (posT ?c - crate ?t - truck)
    (onP ?c - crate ?s - pallet)
    (onC ?c - crate ?s - crate)
    (onH ?c - crate ?h - hoist)
    (onT ?c - crate ?t - truck)
  )
    
  (:action Drive
   :parameters (?t - truck ?x - place ?y - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      ;;(= (at ?t) ?x)
      (at ?t ?x)
    )
   :effect 
    (and 
      ;; (assign (at ?t) ?y)
      (at ?t ?y)
      (not (at ?t ?x))
    )
  )
  
  (:action Load
   :parameters (?h - hoist ?c - crate ?t - truck ?p - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      ;;(= (at ?t) ?p) 
      (at ?t ?p) 
      ;;(= (pos ?c) ?p)
      (posP ?c ?p)
      (not (clearC ?c)) 
      (not (clearH ?h))
      ;;(= (on ?c) ?h) 
      (onH ?c ?h) 
      (located ?h ?p)
    )
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      ;;(assign (pos ?c) ?t)
      (posT ?c ?t)
      (not (posP ?c ?p))
      ;;(assign (on ?c) ?t)
      (onT ?c ?t)
      (not (onH ?c ?h))
    )
  )
  
  (:action Unload
   :parameters (?h - hoist ?c - crate ?t - truck ?p - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      (located ?h ?p) 
      (at ?t ?p)
      ;;(= (pos ?c) ?t) 
      (posT ?c ?t) 
      ;;(= (on ?c) ?t) 
      (onT ?c ?t) 
      (clearH ?h)
      (clearC ?c))
   :effect 
    (and 
      ;;(assign (pos ?c) ?p) 
      (posP ?c ?p) 
      (not (posT ?c ?t))
      ;; (assign (on ?c) ?h)
      (onH ?c ?h)
      (not (onT ?c ?t))
      (not (clearC ?c)) 
      (not (clearH ?h))
    )
  )
)