(define 
(problem depotprob1818)
	(:domain depot)
	(:objects
	 depot0 - place
	 distributor0 distributor1 - place
	 truck0 truck1 - truck
	 crate0 crate1 - crate
	 pallet0 pallet1 pallet2 - pallet
	 hoist0 hoist1 hoist2 - hoist
	)
	
	(:init
	 (myAgent distributor1)
	 (posP crate0 distributor0)
	 (clearC crate0)
	 (onP crate0 pallet1)
	 (posP crate1 depot0)
	 (clearC crate1)
	 (onP crate1 pallet0)
	 (at truck0 distributor1)
	 (at truck1 depot0)
	 (located hoist0 depot0)
	 (clearH hoist0)
	 (located hoist1 distributor0)
	 (clearH hoist1)
	 (located hoist2 distributor1)
	 (clearH hoist2)
	 (placed pallet0 depot0)
	 (not (clearP pallet0))
	 (placed pallet1 distributor0)
	 (not (clearP pallet1))
	 (placed pallet2 distributor1)
	 (clearP pallet2)
	)
	(:goal 
		(and
	 		(onP crate0 pallet2)
	 		(onP crate1 pallet1)
		)
	)
)