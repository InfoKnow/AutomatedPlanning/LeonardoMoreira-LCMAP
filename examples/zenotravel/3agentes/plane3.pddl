(define 
	(problem ZTRAVEL-2-4)
	(:domain zeno-travel)
	(:objects
		 plane3 - aircraft
		 person1 person2 person3 person4 person5 person6 - person
		 ;; person3 person6 - person
		 city0 city1 city2 city3 city4 - city
		 fl0 fl1 fl2 fl3 fl4 fl5 fl6 - flevel
	)
	
	(:init
	 (myAgent plane3)
	 (fuel-level plane3 fl3)
	 (at plane3 city0)
	 (inC person1 city0)
	 (inC person2 city0)
	 (inC person3 city2)
	 (inC person4 city0)
	 (inC person5 city3)
	 (inC person6 city4)
	 (next fl0 fl1)
	 (next fl1 fl2)
	 (next fl2 fl3)
	 (next fl3 fl4)
	 (next fl4 fl5)
	 (next fl5 fl6)
	)
	(:goal 
		(and
		 (inC person1 city0)
		 (inC person2 city0)
		 (inC person3 city1)
		 (inC person4 city0)
		 (inC person5 city3)
		 (inC person6 city2)
		)
	)
)