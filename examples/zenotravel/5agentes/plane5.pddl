(define 
	(problem ZTRAVEL-2-4)
	(:domain zeno-travel)
	(:objects
		 plane5 - aircraft
		 person1 person2 person3 person4 person5 person6 person7 person8 person9 person10 - person
		 city0 city1 city2 city3 city4 city5 city6 city7 city8 city9 - city
		 fl0 fl1 fl2 fl3 fl4 fl5 fl6 - flevel
	)
	
	(:init
	 (myAgent plane5)
	 (at plane5 city9)
	 (fuel-level plane5 fl4)
	 (inC person1 city9)
	 (inC person2 city1)
	 (inC person3 city0)
	 (inC person4 city9)
	 (inC person5 city6)
	 (inC person6 city0)
	 (inC person7 city7)
	 (inC person8 city6)
	 (inC person9 city4)
	 (inC person10 city7)
	 (next fl0 fl1)
	 (next fl1 fl2)
	 (next fl2 fl3)
	 (next fl3 fl4)
	 (next fl4 fl5)
	 (next fl5 fl6)
	)
	(:goal 
		(and
		 (at plane5 city8)
		 (inC person2 city8)
		 (inC person3 city2)
		 (inC person4 city7)
		 (inC person5 city1)
		 (inC person6 city6)
		 (inC person7 city5)
		 (inC person8 city1)
		 (inC person9 city5)
		 (inC person10 city9)
		)
	)
)