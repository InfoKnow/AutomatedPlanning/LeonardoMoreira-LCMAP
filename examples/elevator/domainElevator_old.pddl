(define 
  (domain elevators)
  (:requirements :typing)
  (:types passenger count - object
          elevator - agent
          slow-elevator fast-elevator - elevator
  )
  (:predicates 
    (myAgent ?e - elevator)
  	(reachable-floor ?lift - elevator ?floor - count)
  	(above ?floor1 - count ?floor2 - count)
  	(can-hold ?lift - elevator ?n - count)
    (atC ?person - passenger ?c - count)
    (atE ?person - passenger ?e - elevator)
    (lift-at ?lift - elevator ?c - count)
    (passengers ?lift - elevato ?c - count)
    (next ?n - count ?c - count)
  )
  
  (:action move-up-slow
    :parameters (?lift - slow-elevator ?f1 - count ?f2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f1)
        (lift-at ?lift ?f1)
        (above ?f1 ?f2) 
        (reachable-floor ?lift ?f2)
      )
    :effect 
      (and 
        ;;(assign (lift-at ?lift) ?f2)
          (lift-at ?lift ?f2)
          (not (lift-at ?lift ?f1))
      )
  )
  (:action move-down-slow
    :parameters (?lift - slow-elevator ?f1 - count ?f2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f1) 
        (lift-at ?lift ?f1) 
        (above ?f2 ?f1) 
        (reachable-floor ?lift ?f2)
      )
    :effect 
      (and 
        ;;(assign (lift-at ?lift) ?f2)
        (lift-at ?lift ?f2)
        (not (lift-at ?lift ?f1))
      )
  )
  (:action move-up-fast
    :parameters (?lift - fast-elevator ?f1 - count ?f2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f1) 
        (lift-at ?lift ?f1) 
        (above ?f1 ?f2) 
        (reachable-floor ?lift ?f2)
      )
    :effect 
      (and 
        ;;(assign (lift-at ?lift) ?f2)
        (lift-at ?lift ?f2)
        (not (lift-at ?lift ?f1))
      )
  )
  (:action move-down-fast
    :parameters (?lift - fast-elevator ?f1 - count ?f2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f1) 
        (lift-at ?lift ?f1) 
        (above ?f2 ?f1) 
        (reachable-floor ?lift ?f2)
      )
    :effect 
      (and 
        ;;(assign (lift-at ?lift) ?f2)
        (lift-at ?lift ?f2)
        (not (lift-at ?lift ?f1))
      )
  )
  (:action board
    :parameters (?p - passenger ?lift - elevator ?f - count ?n1 - count ?n2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f) 
        (lift-at ?lift ?f) 
        ;;(= (at ?p) ?f) 
        (atC ?p ?f) 
        ;;(= (passengers ?lift) ?n1)
        (passengers ?lift ?n1) 
        ;;(= (next ?n1) ?n2) 
        (next ?n1 ?n2) 
        (can-hold ?lift ?n2)
      )
    :effect 
      (and 
        ;;(assign (at ?p) ?lift)
        (atE ?p ?lift) 
        (not (atC ?p ?f))
        ;;(assign (passengers ?lift) ?n2)
        (passengers ?lift ?n2)
        (not (passengers ?lift ?n1))
      )
  )
  (:action leave
    :parameters (?p - passenger ?lift - elevator ?f - count ?n1 - count ?n2 - count)
    :precondition 
      (and 
        (myAgent ?lift) 
        ;;(= (lift-at ?lift) ?f)
        (lift-at ?lift ?f) 
        ;;(= (at ?p) ?lift) 
        (atE ?p ?lift) 
        ;;(= (passengers ?lift) ?n1)
        (passengers ?lift ?n1) 
        ;;(= (next ?n2) ?n1)
        (next ?n2 ?n1)
      )
    :effect 
      (and 
        ;;(assign (at ?p) ?f)
        (atC ?p ?f) 
        (not (atE ?p ?lift))
        ;;(assign (passengers ?lift) ?n2)
        (passengers ?lift ?n2)
        (not (passengers ?lift ?n1))
      )
  )
)