import argparse
from models.InputHandler2 import *
from models.Jarvis import *
import time

if __name__ == '__main__':

    
    parser = argparse.ArgumentParser(description='MAP3 - Multi Agent Planner with Pre-planning')
    parser.add_argument('inputFile')
    parser.add_argument('-p',action='store_true',help='execucao em paralelo')
    parser.add_argument('-lb',action='store_true',help='estrategia de delegacao: balanceamento de carga')
    args = parser.parse_args()
    startI = time.time()
    inputHandler = InputHandler2(args)
    verificacao = time.time() - startI
    
    jarvis = Jarvis(args,inputHandler.getInputs())
    coordination = jarvis.coordinate()
    total = time.time() - startI

    print('{},{},{},{}'.format(verificacao+coordination[0],coordination[1],coordination[2],total))

