﻿(define 
	(problem logistics-4-0)
	(:domain logistics)
	(:objects
 		plane1 - plane
 		truck1 truck2 truck3 - truck
 		apt1 apt2 apt3 pos3 pos2 pos1 - location
 		cit3 cit2 cit1 - city
 		obj33 obj32 obj31 obj23 obj22 obj21 obj13 obj12 obj11 - package
	)

	(:init 

		(atPlane plane1 apt1)
		(atTruck truck1 pos1)
		(atTruck truck2 pos2)
		(atTruck truck3 pos3)
		(atPackage obj33 pos3)
		(atPackage obj32 pos3)
		(atPackage obj31 pos3)
		(atPackage obj23 pos2)
		(atPackage obj22 pos2)
		(atPackage obj21 pos2)
		(atPackage obj13 pos1)
		(atPackage obj12 pos1)
		(atPackage obj11 pos1)
		(inCity apt3 cit3)
		(inCity apt2 cit2)
		(inCity apt1 cit1)
		(inCity pos3 cit3)
		(inCity pos2 cit2)
		(inCity pos1 cit1)
		(isAirport apt1)
		(isAirport apt2)
		(isAirport apt3)
		
	)
	(:goal 
		(and
	 		(atPackage obj11 pos3)
	 		(atPackage obj21 pos2)
	 		(atPackage obj31 apt3)
	 		(atPackage obj22 pos3)
	 		(atPackage obj12 pos1)
	 		(atPackage obj23 apt2)
	 		(atPackage obj13 apt2)
	 		(atPackage obj32 apt1)
		)
	)
)
