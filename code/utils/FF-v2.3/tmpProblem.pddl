(define
	(problem tmpProblem)
	(:domain tmpDomain)
	(:objects
		cit1 cit2 - city
		truck1 truck2 - truck
		plane1 - plane
		obj11 obj13 obj22 obj23 obj21 obj12 - package
		apt2 pos1 apt1 pos2 - location
	)
	(:init
		(inCity pos2 cit2)
		(inCity apt1 cit1)
		(atPackage obj23 pos2)
		(atTruck truck2 pos2)
		(atPackage obj13 pos1)
		(atPackage obj11 pos1)
		(isAirport apt2)
		(inCity pos1 cit1)
		(atPackage obj12 pos1)
		(atPlane plane1 apt1)
		(atTruck truck1 pos1)
		(isAirport apt1)
		(atPackage obj22 pos2)
		(inCity apt2 cit2)
		(atPackage obj21 pos2)
	)
	(:goal
		(and
			(atPackage obj11 apt1)
			(atPackage obj23 pos1)
			(atPackage obj13 apt1)
			(atPackage obj21 pos1)
		)
	)
)