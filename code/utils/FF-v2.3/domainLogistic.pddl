(
  define (domain logistics)
  (:requirements :typing)
  (:types location city airport package plane truck)

  (:predicates 
    (atPackage ?p - package ?l - location)
    (atPlane ?plane - plane ?l - location)
    (inPlane ?p - package ?plane - plane)
    (inCity ?loc - location ?city - city)
    (isAirport ?loc - location)
    (atTruck ?truck - truck ?loc - location)
    (inTruck ?p - package ?truck - truck)
  )

  (:action loadPlane
    :parameters (?p - package  ?loc - location ?plane - plane)
    :precondition 
    (and
      (atPackage ?p ?loc) (atPlane ?plane ?loc)
    )
    :effect
    (and
      (not(atPackage ?p ?loc)) (inPlane ?p ?plane)
    )
  )

  (:action unloadPlane
    :parameters (?p - package  ?loc - location ?plane - plane)
    :precondition
    (and
      (inPlane ?p ?plane) (atPlane ?plane ?loc)
    )
    :effect
    (and
      (not(inPlane ?p ?plane)) (atPackage ?p ?loc)
    )
  )

  (:action flyPlane
    :parameters (?plane - plane ?locFrom - location ?locTo - location)
    :precondition
    (and
      (atPlane ?plane ?locFrom) (isAirport ?locFrom) (isAirport ?locTo)
    )
    :effect
    (and
      (not(atPlane ?plane ?locFrom)) (atPlane ?plane ?locTo)
    )
  )

  (:action loadTruck
    :parameters (?p - package  ?loc - location ?truck - truck)
    :precondition 
    (and
      (atPackage ?p ?loc) (atTruck ?truck ?loc)
    )
    :effect
    (and
      (not(atPackage ?p ?loc)) (inTruck ?p ?truck)
    )
  )

  (:action unloadTruck
    :parameters (?p - package  ?loc - location ?truck - truck)
    :precondition
    (and
      (inTruck ?p ?truck) (atTruck ?truck ?loc)
    )
    :effect
    (and
      (not(inTruck ?p ?truck)) (atPackage ?p ?loc)
    )
  )

  (:action driveTruck
    :parameters (?truck - truck ?locFrom - location ?locTo - location ?city - city)
    :precondition
    (and
      (atTruck ?truck ?locFrom) (inCity ?locFrom ?city) (inCity ?locTo ?city)
    )
    :effect
    (and
      (not(atTruck ?truck ?locFrom)) (atTruck ?truck ?locTo)
    )
  )

)  
