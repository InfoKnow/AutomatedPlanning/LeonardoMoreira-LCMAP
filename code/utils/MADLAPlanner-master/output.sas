begin_version
3
end_version
begin_metric
0
end_metric
8
begin_variable
var0
-1
3
Atom at(plane1, city0)
Atom at(plane1, city1)
Atom at(plane1, city2)
end_variable
begin_variable
var1
-1
3
Atom at(plane2, city0)
Atom at(plane2, city1)
Atom at(plane2, city2)
end_variable
begin_variable
var2
-1
7
Atom fuel-level(plane1, fl0)
Atom fuel-level(plane1, fl1)
Atom fuel-level(plane1, fl2)
Atom fuel-level(plane1, fl3)
Atom fuel-level(plane1, fl4)
Atom fuel-level(plane1, fl5)
Atom fuel-level(plane1, fl6)
end_variable
begin_variable
var3
-1
7
Atom fuel-level(plane2, fl0)
Atom fuel-level(plane2, fl1)
Atom fuel-level(plane2, fl2)
Atom fuel-level(plane2, fl3)
Atom fuel-level(plane2, fl4)
Atom fuel-level(plane2, fl5)
Atom fuel-level(plane2, fl6)
end_variable
begin_variable
var4
-1
5
Atom ina(person1, plane1)
Atom ina(person1, plane2)
Atom inc(person1, city0)
Atom inc(person1, city1)
Atom inc(person1, city2)
end_variable
begin_variable
var5
-1
5
Atom ina(person2, plane1)
Atom ina(person2, plane2)
Atom inc(person2, city0)
Atom inc(person2, city1)
Atom inc(person2, city2)
end_variable
begin_variable
var6
-1
5
Atom ina(person3, plane1)
Atom ina(person3, plane2)
Atom inc(person3, city0)
Atom inc(person3, city1)
Atom inc(person3, city2)
end_variable
begin_variable
var7
-1
5
Atom ina(person4, plane1)
Atom ina(person4, plane2)
Atom inc(person4, city0)
Atom inc(person4, city1)
Atom inc(person4, city2)
end_variable
8
begin_mutex_group
3
0 0
0 1
0 2
end_mutex_group
begin_mutex_group
3
1 0
1 1
1 2
end_mutex_group
begin_mutex_group
7
2 0
2 1
2 2
2 3
2 4
2 5
2 6
end_mutex_group
begin_mutex_group
7
3 0
3 1
3 2
3 3
3 4
3 5
3 6
end_mutex_group
begin_mutex_group
5
4 0
4 1
4 2
4 3
4 4
end_mutex_group
begin_mutex_group
5
5 0
5 1
5 2
5 3
5 4
end_mutex_group
begin_mutex_group
5
6 0
6 1
6 2
6 3
6 4
end_mutex_group
begin_mutex_group
5
7 0
7 1
7 2
7 3
7 4
end_mutex_group
begin_state
0
2
4
5
2
2
3
3
end_state
begin_goal
5
1 2
4 3
5 2
6 2
7 3
end_goal
282
begin_operator
board person1 plane1 city0
1
0 0
1
0 4 2 0
1
end_operator
begin_operator
board person1 plane1 city1
1
0 1
1
0 4 3 0
1
end_operator
begin_operator
board person1 plane1 city2
1
0 2
1
0 4 4 0
1
end_operator
begin_operator
board person1 plane2 city0
1
1 0
1
0 4 2 1
1
end_operator
begin_operator
board person1 plane2 city1
1
1 1
1
0 4 3 1
1
end_operator
begin_operator
board person1 plane2 city2
1
1 2
1
0 4 4 1
1
end_operator
begin_operator
board person2 plane1 city0
1
0 0
1
0 5 2 0
1
end_operator
begin_operator
board person2 plane1 city1
1
0 1
1
0 5 3 0
1
end_operator
begin_operator
board person2 plane1 city2
1
0 2
1
0 5 4 0
1
end_operator
begin_operator
board person2 plane2 city0
1
1 0
1
0 5 2 1
1
end_operator
begin_operator
board person2 plane2 city1
1
1 1
1
0 5 3 1
1
end_operator
begin_operator
board person2 plane2 city2
1
1 2
1
0 5 4 1
1
end_operator
begin_operator
board person3 plane1 city0
1
0 0
1
0 6 2 0
1
end_operator
begin_operator
board person3 plane1 city1
1
0 1
1
0 6 3 0
1
end_operator
begin_operator
board person3 plane1 city2
1
0 2
1
0 6 4 0
1
end_operator
begin_operator
board person3 plane2 city0
1
1 0
1
0 6 2 1
1
end_operator
begin_operator
board person3 plane2 city1
1
1 1
1
0 6 3 1
1
end_operator
begin_operator
board person3 plane2 city2
1
1 2
1
0 6 4 1
1
end_operator
begin_operator
board person4 plane1 city0
1
0 0
1
0 7 2 0
1
end_operator
begin_operator
board person4 plane1 city1
1
0 1
1
0 7 3 0
1
end_operator
begin_operator
board person4 plane1 city2
1
0 2
1
0 7 4 0
1
end_operator
begin_operator
board person4 plane2 city0
1
1 0
1
0 7 2 1
1
end_operator
begin_operator
board person4 plane2 city1
1
1 1
1
0 7 3 1
1
end_operator
begin_operator
board person4 plane2 city2
1
1 2
1
0 7 4 1
1
end_operator
begin_operator
debark person1 plane1 city0
1
0 0
1
0 4 0 2
1
end_operator
begin_operator
debark person1 plane1 city1
1
0 1
1
0 4 0 3
1
end_operator
begin_operator
debark person1 plane1 city2
1
0 2
1
0 4 0 4
1
end_operator
begin_operator
debark person1 plane2 city0
1
1 0
1
0 4 1 2
1
end_operator
begin_operator
debark person1 plane2 city1
1
1 1
1
0 4 1 3
1
end_operator
begin_operator
debark person1 plane2 city2
1
1 2
1
0 4 1 4
1
end_operator
begin_operator
debark person2 plane1 city0
1
0 0
1
0 5 0 2
1
end_operator
begin_operator
debark person2 plane1 city1
1
0 1
1
0 5 0 3
1
end_operator
begin_operator
debark person2 plane1 city2
1
0 2
1
0 5 0 4
1
end_operator
begin_operator
debark person2 plane2 city0
1
1 0
1
0 5 1 2
1
end_operator
begin_operator
debark person2 plane2 city1
1
1 1
1
0 5 1 3
1
end_operator
begin_operator
debark person2 plane2 city2
1
1 2
1
0 5 1 4
1
end_operator
begin_operator
debark person3 plane1 city0
1
0 0
1
0 6 0 2
1
end_operator
begin_operator
debark person3 plane1 city1
1
0 1
1
0 6 0 3
1
end_operator
begin_operator
debark person3 plane1 city2
1
0 2
1
0 6 0 4
1
end_operator
begin_operator
debark person3 plane2 city0
1
1 0
1
0 6 1 2
1
end_operator
begin_operator
debark person3 plane2 city1
1
1 1
1
0 6 1 3
1
end_operator
begin_operator
debark person3 plane2 city2
1
1 2
1
0 6 1 4
1
end_operator
begin_operator
debark person4 plane1 city0
1
0 0
1
0 7 0 2
1
end_operator
begin_operator
debark person4 plane1 city1
1
0 1
1
0 7 0 3
1
end_operator
begin_operator
debark person4 plane1 city2
1
0 2
1
0 7 0 4
1
end_operator
begin_operator
debark person4 plane2 city0
1
1 0
1
0 7 1 2
1
end_operator
begin_operator
debark person4 plane2 city1
1
1 1
1
0 7 1 3
1
end_operator
begin_operator
debark person4 plane2 city2
1
1 2
1
0 7 1 4
1
end_operator
begin_operator
fly plane1 city0 city0 fl1 fl0
1
0 0
1
0 2 1 0
1
end_operator
begin_operator
fly plane1 city0 city0 fl2 fl1
1
0 0
1
0 2 2 1
1
end_operator
begin_operator
fly plane1 city0 city0 fl3 fl2
1
0 0
1
0 2 3 2
1
end_operator
begin_operator
fly plane1 city0 city0 fl4 fl3
1
0 0
1
0 2 4 3
1
end_operator
begin_operator
fly plane1 city0 city0 fl5 fl4
1
0 0
1
0 2 5 4
1
end_operator
begin_operator
fly plane1 city0 city0 fl6 fl5
1
0 0
1
0 2 6 5
1
end_operator
begin_operator
fly plane1 city0 city1 fl1 fl0
0
2
0 0 0 1
0 2 1 0
1
end_operator
begin_operator
fly plane1 city0 city1 fl2 fl1
0
2
0 0 0 1
0 2 2 1
1
end_operator
begin_operator
fly plane1 city0 city1 fl3 fl2
0
2
0 0 0 1
0 2 3 2
1
end_operator
begin_operator
fly plane1 city0 city1 fl4 fl3
0
2
0 0 0 1
0 2 4 3
1
end_operator
begin_operator
fly plane1 city0 city1 fl5 fl4
0
2
0 0 0 1
0 2 5 4
1
end_operator
begin_operator
fly plane1 city0 city1 fl6 fl5
0
2
0 0 0 1
0 2 6 5
1
end_operator
begin_operator
fly plane1 city0 city2 fl1 fl0
0
2
0 0 0 2
0 2 1 0
1
end_operator
begin_operator
fly plane1 city0 city2 fl2 fl1
0
2
0 0 0 2
0 2 2 1
1
end_operator
begin_operator
fly plane1 city0 city2 fl3 fl2
0
2
0 0 0 2
0 2 3 2
1
end_operator
begin_operator
fly plane1 city0 city2 fl4 fl3
0
2
0 0 0 2
0 2 4 3
1
end_operator
begin_operator
fly plane1 city0 city2 fl5 fl4
0
2
0 0 0 2
0 2 5 4
1
end_operator
begin_operator
fly plane1 city0 city2 fl6 fl5
0
2
0 0 0 2
0 2 6 5
1
end_operator
begin_operator
fly plane1 city1 city0 fl1 fl0
0
2
0 0 1 0
0 2 1 0
1
end_operator
begin_operator
fly plane1 city1 city0 fl2 fl1
0
2
0 0 1 0
0 2 2 1
1
end_operator
begin_operator
fly plane1 city1 city0 fl3 fl2
0
2
0 0 1 0
0 2 3 2
1
end_operator
begin_operator
fly plane1 city1 city0 fl4 fl3
0
2
0 0 1 0
0 2 4 3
1
end_operator
begin_operator
fly plane1 city1 city0 fl5 fl4
0
2
0 0 1 0
0 2 5 4
1
end_operator
begin_operator
fly plane1 city1 city0 fl6 fl5
0
2
0 0 1 0
0 2 6 5
1
end_operator
begin_operator
fly plane1 city1 city1 fl1 fl0
1
0 1
1
0 2 1 0
1
end_operator
begin_operator
fly plane1 city1 city1 fl2 fl1
1
0 1
1
0 2 2 1
1
end_operator
begin_operator
fly plane1 city1 city1 fl3 fl2
1
0 1
1
0 2 3 2
1
end_operator
begin_operator
fly plane1 city1 city1 fl4 fl3
1
0 1
1
0 2 4 3
1
end_operator
begin_operator
fly plane1 city1 city1 fl5 fl4
1
0 1
1
0 2 5 4
1
end_operator
begin_operator
fly plane1 city1 city1 fl6 fl5
1
0 1
1
0 2 6 5
1
end_operator
begin_operator
fly plane1 city1 city2 fl1 fl0
0
2
0 0 1 2
0 2 1 0
1
end_operator
begin_operator
fly plane1 city1 city2 fl2 fl1
0
2
0 0 1 2
0 2 2 1
1
end_operator
begin_operator
fly plane1 city1 city2 fl3 fl2
0
2
0 0 1 2
0 2 3 2
1
end_operator
begin_operator
fly plane1 city1 city2 fl4 fl3
0
2
0 0 1 2
0 2 4 3
1
end_operator
begin_operator
fly plane1 city1 city2 fl5 fl4
0
2
0 0 1 2
0 2 5 4
1
end_operator
begin_operator
fly plane1 city1 city2 fl6 fl5
0
2
0 0 1 2
0 2 6 5
1
end_operator
begin_operator
fly plane1 city2 city0 fl1 fl0
0
2
0 0 2 0
0 2 1 0
1
end_operator
begin_operator
fly plane1 city2 city0 fl2 fl1
0
2
0 0 2 0
0 2 2 1
1
end_operator
begin_operator
fly plane1 city2 city0 fl3 fl2
0
2
0 0 2 0
0 2 3 2
1
end_operator
begin_operator
fly plane1 city2 city0 fl4 fl3
0
2
0 0 2 0
0 2 4 3
1
end_operator
begin_operator
fly plane1 city2 city0 fl5 fl4
0
2
0 0 2 0
0 2 5 4
1
end_operator
begin_operator
fly plane1 city2 city0 fl6 fl5
0
2
0 0 2 0
0 2 6 5
1
end_operator
begin_operator
fly plane1 city2 city1 fl1 fl0
0
2
0 0 2 1
0 2 1 0
1
end_operator
begin_operator
fly plane1 city2 city1 fl2 fl1
0
2
0 0 2 1
0 2 2 1
1
end_operator
begin_operator
fly plane1 city2 city1 fl3 fl2
0
2
0 0 2 1
0 2 3 2
1
end_operator
begin_operator
fly plane1 city2 city1 fl4 fl3
0
2
0 0 2 1
0 2 4 3
1
end_operator
begin_operator
fly plane1 city2 city1 fl5 fl4
0
2
0 0 2 1
0 2 5 4
1
end_operator
begin_operator
fly plane1 city2 city1 fl6 fl5
0
2
0 0 2 1
0 2 6 5
1
end_operator
begin_operator
fly plane1 city2 city2 fl1 fl0
1
0 2
1
0 2 1 0
1
end_operator
begin_operator
fly plane1 city2 city2 fl2 fl1
1
0 2
1
0 2 2 1
1
end_operator
begin_operator
fly plane1 city2 city2 fl3 fl2
1
0 2
1
0 2 3 2
1
end_operator
begin_operator
fly plane1 city2 city2 fl4 fl3
1
0 2
1
0 2 4 3
1
end_operator
begin_operator
fly plane1 city2 city2 fl5 fl4
1
0 2
1
0 2 5 4
1
end_operator
begin_operator
fly plane1 city2 city2 fl6 fl5
1
0 2
1
0 2 6 5
1
end_operator
begin_operator
fly plane2 city0 city0 fl1 fl0
1
1 0
1
0 3 1 0
1
end_operator
begin_operator
fly plane2 city0 city0 fl2 fl1
1
1 0
1
0 3 2 1
1
end_operator
begin_operator
fly plane2 city0 city0 fl3 fl2
1
1 0
1
0 3 3 2
1
end_operator
begin_operator
fly plane2 city0 city0 fl4 fl3
1
1 0
1
0 3 4 3
1
end_operator
begin_operator
fly plane2 city0 city0 fl5 fl4
1
1 0
1
0 3 5 4
1
end_operator
begin_operator
fly plane2 city0 city0 fl6 fl5
1
1 0
1
0 3 6 5
1
end_operator
begin_operator
fly plane2 city0 city1 fl1 fl0
0
2
0 1 0 1
0 3 1 0
1
end_operator
begin_operator
fly plane2 city0 city1 fl2 fl1
0
2
0 1 0 1
0 3 2 1
1
end_operator
begin_operator
fly plane2 city0 city1 fl3 fl2
0
2
0 1 0 1
0 3 3 2
1
end_operator
begin_operator
fly plane2 city0 city1 fl4 fl3
0
2
0 1 0 1
0 3 4 3
1
end_operator
begin_operator
fly plane2 city0 city1 fl5 fl4
0
2
0 1 0 1
0 3 5 4
1
end_operator
begin_operator
fly plane2 city0 city1 fl6 fl5
0
2
0 1 0 1
0 3 6 5
1
end_operator
begin_operator
fly plane2 city0 city2 fl1 fl0
0
2
0 1 0 2
0 3 1 0
1
end_operator
begin_operator
fly plane2 city0 city2 fl2 fl1
0
2
0 1 0 2
0 3 2 1
1
end_operator
begin_operator
fly plane2 city0 city2 fl3 fl2
0
2
0 1 0 2
0 3 3 2
1
end_operator
begin_operator
fly plane2 city0 city2 fl4 fl3
0
2
0 1 0 2
0 3 4 3
1
end_operator
begin_operator
fly plane2 city0 city2 fl5 fl4
0
2
0 1 0 2
0 3 5 4
1
end_operator
begin_operator
fly plane2 city0 city2 fl6 fl5
0
2
0 1 0 2
0 3 6 5
1
end_operator
begin_operator
fly plane2 city1 city0 fl1 fl0
0
2
0 1 1 0
0 3 1 0
1
end_operator
begin_operator
fly plane2 city1 city0 fl2 fl1
0
2
0 1 1 0
0 3 2 1
1
end_operator
begin_operator
fly plane2 city1 city0 fl3 fl2
0
2
0 1 1 0
0 3 3 2
1
end_operator
begin_operator
fly plane2 city1 city0 fl4 fl3
0
2
0 1 1 0
0 3 4 3
1
end_operator
begin_operator
fly plane2 city1 city0 fl5 fl4
0
2
0 1 1 0
0 3 5 4
1
end_operator
begin_operator
fly plane2 city1 city0 fl6 fl5
0
2
0 1 1 0
0 3 6 5
1
end_operator
begin_operator
fly plane2 city1 city1 fl1 fl0
1
1 1
1
0 3 1 0
1
end_operator
begin_operator
fly plane2 city1 city1 fl2 fl1
1
1 1
1
0 3 2 1
1
end_operator
begin_operator
fly plane2 city1 city1 fl3 fl2
1
1 1
1
0 3 3 2
1
end_operator
begin_operator
fly plane2 city1 city1 fl4 fl3
1
1 1
1
0 3 4 3
1
end_operator
begin_operator
fly plane2 city1 city1 fl5 fl4
1
1 1
1
0 3 5 4
1
end_operator
begin_operator
fly plane2 city1 city1 fl6 fl5
1
1 1
1
0 3 6 5
1
end_operator
begin_operator
fly plane2 city1 city2 fl1 fl0
0
2
0 1 1 2
0 3 1 0
1
end_operator
begin_operator
fly plane2 city1 city2 fl2 fl1
0
2
0 1 1 2
0 3 2 1
1
end_operator
begin_operator
fly plane2 city1 city2 fl3 fl2
0
2
0 1 1 2
0 3 3 2
1
end_operator
begin_operator
fly plane2 city1 city2 fl4 fl3
0
2
0 1 1 2
0 3 4 3
1
end_operator
begin_operator
fly plane2 city1 city2 fl5 fl4
0
2
0 1 1 2
0 3 5 4
1
end_operator
begin_operator
fly plane2 city1 city2 fl6 fl5
0
2
0 1 1 2
0 3 6 5
1
end_operator
begin_operator
fly plane2 city2 city0 fl1 fl0
0
2
0 1 2 0
0 3 1 0
1
end_operator
begin_operator
fly plane2 city2 city0 fl2 fl1
0
2
0 1 2 0
0 3 2 1
1
end_operator
begin_operator
fly plane2 city2 city0 fl3 fl2
0
2
0 1 2 0
0 3 3 2
1
end_operator
begin_operator
fly plane2 city2 city0 fl4 fl3
0
2
0 1 2 0
0 3 4 3
1
end_operator
begin_operator
fly plane2 city2 city0 fl5 fl4
0
2
0 1 2 0
0 3 5 4
1
end_operator
begin_operator
fly plane2 city2 city0 fl6 fl5
0
2
0 1 2 0
0 3 6 5
1
end_operator
begin_operator
fly plane2 city2 city1 fl1 fl0
0
2
0 1 2 1
0 3 1 0
1
end_operator
begin_operator
fly plane2 city2 city1 fl2 fl1
0
2
0 1 2 1
0 3 2 1
1
end_operator
begin_operator
fly plane2 city2 city1 fl3 fl2
0
2
0 1 2 1
0 3 3 2
1
end_operator
begin_operator
fly plane2 city2 city1 fl4 fl3
0
2
0 1 2 1
0 3 4 3
1
end_operator
begin_operator
fly plane2 city2 city1 fl5 fl4
0
2
0 1 2 1
0 3 5 4
1
end_operator
begin_operator
fly plane2 city2 city1 fl6 fl5
0
2
0 1 2 1
0 3 6 5
1
end_operator
begin_operator
fly plane2 city2 city2 fl1 fl0
1
1 2
1
0 3 1 0
1
end_operator
begin_operator
fly plane2 city2 city2 fl2 fl1
1
1 2
1
0 3 2 1
1
end_operator
begin_operator
fly plane2 city2 city2 fl3 fl2
1
1 2
1
0 3 3 2
1
end_operator
begin_operator
fly plane2 city2 city2 fl4 fl3
1
1 2
1
0 3 4 3
1
end_operator
begin_operator
fly plane2 city2 city2 fl5 fl4
1
1 2
1
0 3 5 4
1
end_operator
begin_operator
fly plane2 city2 city2 fl6 fl5
1
1 2
1
0 3 6 5
1
end_operator
begin_operator
refuel plane1 city0 fl0 fl1
1
0 0
1
0 2 0 1
1
end_operator
begin_operator
refuel plane1 city0 fl1 fl2
1
0 0
1
0 2 1 2
1
end_operator
begin_operator
refuel plane1 city0 fl2 fl3
1
0 0
1
0 2 2 3
1
end_operator
begin_operator
refuel plane1 city0 fl3 fl4
1
0 0
1
0 2 3 4
1
end_operator
begin_operator
refuel plane1 city0 fl4 fl5
1
0 0
1
0 2 4 5
1
end_operator
begin_operator
refuel plane1 city0 fl5 fl6
1
0 0
1
0 2 5 6
1
end_operator
begin_operator
refuel plane1 city1 fl0 fl1
1
0 1
1
0 2 0 1
1
end_operator
begin_operator
refuel plane1 city1 fl1 fl2
1
0 1
1
0 2 1 2
1
end_operator
begin_operator
refuel plane1 city1 fl2 fl3
1
0 1
1
0 2 2 3
1
end_operator
begin_operator
refuel plane1 city1 fl3 fl4
1
0 1
1
0 2 3 4
1
end_operator
begin_operator
refuel plane1 city1 fl4 fl5
1
0 1
1
0 2 4 5
1
end_operator
begin_operator
refuel plane1 city1 fl5 fl6
1
0 1
1
0 2 5 6
1
end_operator
begin_operator
refuel plane1 city2 fl0 fl1
1
0 2
1
0 2 0 1
1
end_operator
begin_operator
refuel plane1 city2 fl1 fl2
1
0 2
1
0 2 1 2
1
end_operator
begin_operator
refuel plane1 city2 fl2 fl3
1
0 2
1
0 2 2 3
1
end_operator
begin_operator
refuel plane1 city2 fl3 fl4
1
0 2
1
0 2 3 4
1
end_operator
begin_operator
refuel plane1 city2 fl4 fl5
1
0 2
1
0 2 4 5
1
end_operator
begin_operator
refuel plane1 city2 fl5 fl6
1
0 2
1
0 2 5 6
1
end_operator
begin_operator
refuel plane2 city0 fl0 fl1
1
1 0
1
0 3 0 1
1
end_operator
begin_operator
refuel plane2 city0 fl1 fl2
1
1 0
1
0 3 1 2
1
end_operator
begin_operator
refuel plane2 city0 fl2 fl3
1
1 0
1
0 3 2 3
1
end_operator
begin_operator
refuel plane2 city0 fl3 fl4
1
1 0
1
0 3 3 4
1
end_operator
begin_operator
refuel plane2 city0 fl4 fl5
1
1 0
1
0 3 4 5
1
end_operator
begin_operator
refuel plane2 city0 fl5 fl6
1
1 0
1
0 3 5 6
1
end_operator
begin_operator
refuel plane2 city1 fl0 fl1
1
1 1
1
0 3 0 1
1
end_operator
begin_operator
refuel plane2 city1 fl1 fl2
1
1 1
1
0 3 1 2
1
end_operator
begin_operator
refuel plane2 city1 fl2 fl3
1
1 1
1
0 3 2 3
1
end_operator
begin_operator
refuel plane2 city1 fl3 fl4
1
1 1
1
0 3 3 4
1
end_operator
begin_operator
refuel plane2 city1 fl4 fl5
1
1 1
1
0 3 4 5
1
end_operator
begin_operator
refuel plane2 city1 fl5 fl6
1
1 1
1
0 3 5 6
1
end_operator
begin_operator
refuel plane2 city2 fl0 fl1
1
1 2
1
0 3 0 1
1
end_operator
begin_operator
refuel plane2 city2 fl1 fl2
1
1 2
1
0 3 1 2
1
end_operator
begin_operator
refuel plane2 city2 fl2 fl3
1
1 2
1
0 3 2 3
1
end_operator
begin_operator
refuel plane2 city2 fl3 fl4
1
1 2
1
0 3 3 4
1
end_operator
begin_operator
refuel plane2 city2 fl4 fl5
1
1 2
1
0 3 4 5
1
end_operator
begin_operator
refuel plane2 city2 fl5 fl6
1
1 2
1
0 3 5 6
1
end_operator
begin_operator
zoom plane1 city0 city0 fl2 fl1 fl0
1
0 0
1
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city0 city0 fl3 fl2 fl1
1
0 0
1
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city0 city0 fl4 fl3 fl2
1
0 0
1
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city0 city0 fl5 fl4 fl3
1
0 0
1
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city0 city0 fl6 fl5 fl4
1
0 0
1
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city0 city1 fl2 fl1 fl0
0
2
0 0 0 1
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city0 city1 fl3 fl2 fl1
0
2
0 0 0 1
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city0 city1 fl4 fl3 fl2
0
2
0 0 0 1
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city0 city1 fl5 fl4 fl3
0
2
0 0 0 1
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city0 city1 fl6 fl5 fl4
0
2
0 0 0 1
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city0 city2 fl2 fl1 fl0
0
2
0 0 0 2
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city0 city2 fl3 fl2 fl1
0
2
0 0 0 2
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city0 city2 fl4 fl3 fl2
0
2
0 0 0 2
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city0 city2 fl5 fl4 fl3
0
2
0 0 0 2
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city0 city2 fl6 fl5 fl4
0
2
0 0 0 2
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city1 city0 fl2 fl1 fl0
0
2
0 0 1 0
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city1 city0 fl3 fl2 fl1
0
2
0 0 1 0
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city1 city0 fl4 fl3 fl2
0
2
0 0 1 0
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city1 city0 fl5 fl4 fl3
0
2
0 0 1 0
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city1 city0 fl6 fl5 fl4
0
2
0 0 1 0
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city1 city1 fl2 fl1 fl0
1
0 1
1
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city1 city1 fl3 fl2 fl1
1
0 1
1
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city1 city1 fl4 fl3 fl2
1
0 1
1
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city1 city1 fl5 fl4 fl3
1
0 1
1
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city1 city1 fl6 fl5 fl4
1
0 1
1
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city1 city2 fl2 fl1 fl0
0
2
0 0 1 2
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city1 city2 fl3 fl2 fl1
0
2
0 0 1 2
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city1 city2 fl4 fl3 fl2
0
2
0 0 1 2
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city1 city2 fl5 fl4 fl3
0
2
0 0 1 2
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city1 city2 fl6 fl5 fl4
0
2
0 0 1 2
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city2 city0 fl2 fl1 fl0
0
2
0 0 2 0
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city2 city0 fl3 fl2 fl1
0
2
0 0 2 0
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city2 city0 fl4 fl3 fl2
0
2
0 0 2 0
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city2 city0 fl5 fl4 fl3
0
2
0 0 2 0
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city2 city0 fl6 fl5 fl4
0
2
0 0 2 0
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city2 city1 fl2 fl1 fl0
0
2
0 0 2 1
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city2 city1 fl3 fl2 fl1
0
2
0 0 2 1
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city2 city1 fl4 fl3 fl2
0
2
0 0 2 1
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city2 city1 fl5 fl4 fl3
0
2
0 0 2 1
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city2 city1 fl6 fl5 fl4
0
2
0 0 2 1
0 2 6 4
1
end_operator
begin_operator
zoom plane1 city2 city2 fl2 fl1 fl0
1
0 2
1
0 2 2 0
1
end_operator
begin_operator
zoom plane1 city2 city2 fl3 fl2 fl1
1
0 2
1
0 2 3 1
1
end_operator
begin_operator
zoom plane1 city2 city2 fl4 fl3 fl2
1
0 2
1
0 2 4 2
1
end_operator
begin_operator
zoom plane1 city2 city2 fl5 fl4 fl3
1
0 2
1
0 2 5 3
1
end_operator
begin_operator
zoom plane1 city2 city2 fl6 fl5 fl4
1
0 2
1
0 2 6 4
1
end_operator
begin_operator
zoom plane2 city0 city0 fl2 fl1 fl0
1
1 0
1
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city0 city0 fl3 fl2 fl1
1
1 0
1
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city0 city0 fl4 fl3 fl2
1
1 0
1
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city0 city0 fl5 fl4 fl3
1
1 0
1
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city0 city0 fl6 fl5 fl4
1
1 0
1
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city0 city1 fl2 fl1 fl0
0
2
0 1 0 1
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city0 city1 fl3 fl2 fl1
0
2
0 1 0 1
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city0 city1 fl4 fl3 fl2
0
2
0 1 0 1
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city0 city1 fl5 fl4 fl3
0
2
0 1 0 1
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city0 city1 fl6 fl5 fl4
0
2
0 1 0 1
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city0 city2 fl2 fl1 fl0
0
2
0 1 0 2
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city0 city2 fl3 fl2 fl1
0
2
0 1 0 2
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city0 city2 fl4 fl3 fl2
0
2
0 1 0 2
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city0 city2 fl5 fl4 fl3
0
2
0 1 0 2
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city0 city2 fl6 fl5 fl4
0
2
0 1 0 2
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city1 city0 fl2 fl1 fl0
0
2
0 1 1 0
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city1 city0 fl3 fl2 fl1
0
2
0 1 1 0
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city1 city0 fl4 fl3 fl2
0
2
0 1 1 0
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city1 city0 fl5 fl4 fl3
0
2
0 1 1 0
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city1 city0 fl6 fl5 fl4
0
2
0 1 1 0
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city1 city1 fl2 fl1 fl0
1
1 1
1
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city1 city1 fl3 fl2 fl1
1
1 1
1
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city1 city1 fl4 fl3 fl2
1
1 1
1
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city1 city1 fl5 fl4 fl3
1
1 1
1
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city1 city1 fl6 fl5 fl4
1
1 1
1
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city1 city2 fl2 fl1 fl0
0
2
0 1 1 2
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city1 city2 fl3 fl2 fl1
0
2
0 1 1 2
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city1 city2 fl4 fl3 fl2
0
2
0 1 1 2
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city1 city2 fl5 fl4 fl3
0
2
0 1 1 2
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city1 city2 fl6 fl5 fl4
0
2
0 1 1 2
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city2 city0 fl2 fl1 fl0
0
2
0 1 2 0
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city2 city0 fl3 fl2 fl1
0
2
0 1 2 0
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city2 city0 fl4 fl3 fl2
0
2
0 1 2 0
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city2 city0 fl5 fl4 fl3
0
2
0 1 2 0
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city2 city0 fl6 fl5 fl4
0
2
0 1 2 0
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city2 city1 fl2 fl1 fl0
0
2
0 1 2 1
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city2 city1 fl3 fl2 fl1
0
2
0 1 2 1
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city2 city1 fl4 fl3 fl2
0
2
0 1 2 1
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city2 city1 fl5 fl4 fl3
0
2
0 1 2 1
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city2 city1 fl6 fl5 fl4
0
2
0 1 2 1
0 3 6 4
1
end_operator
begin_operator
zoom plane2 city2 city2 fl2 fl1 fl0
1
1 2
1
0 3 2 0
1
end_operator
begin_operator
zoom plane2 city2 city2 fl3 fl2 fl1
1
1 2
1
0 3 3 1
1
end_operator
begin_operator
zoom plane2 city2 city2 fl4 fl3 fl2
1
1 2
1
0 3 4 2
1
end_operator
begin_operator
zoom plane2 city2 city2 fl5 fl4 fl3
1
1 2
1
0 3 5 3
1
end_operator
begin_operator
zoom plane2 city2 city2 fl6 fl5 fl4
1
1 2
1
0 3 6 4
1
end_operator
0
