(
  define (domain Ladrao)
  (:requirements :typing :negative-preconditions)
  (:types agent room switch diamond door)

  (:predicates 
    (locationA ?agent - agent ?room - room)
    (locationDA ?diamond - diamond ?agent - agent)
    (locationDR ?diamond - diamond ?room - room)
    (doorLocked ?door - door)
    (locationS ?switch - switch ?room - room)

  )

  (:action walkTrough
    :parameters (?agent - agent ?door - door ?fromRoom ?toRoom - room)
    :precondition 
      (and 
        (not (doorLocked ?door)) (locationA ?agent ?fromRoom)
      )
    :effect 
          (and 
              (locationA ?agent ?toRoom)
              (not (locationA ?agent ?fromRoom))
    		  )
  )

  (:action steal
    :parameters (?agent - agent ?diamond - diamond ?room - room ?door - door )
    :precondition 
      (and
        (locationA ?agent ?room) (locationDR ?diamond ?room)
      )
    :effect
      (and
        (doorLocked ?door) (locationDA ?diamond ?agent) (not (locationDR ?diamond ?room))
      )
  )

  (:action switch
    :parameters (?agent - agent ?switch - switch ?room - room ?door - door)
    :precondition
      (and
        (locationA ?agent ?room) (locationS ?switch ?room)
      )
    :effect
      (and
        (not(doorLocked ?door))
      )
  )

  (:action place
    :parameters (?agent - agent ?diamond - diamond ?room - room)
    :precondition
      (and
        (locationA ?agent ?room) (locationDA ?diamond ?agent)
      )
    :effect
      (and
        (locationDR ?diamond ?room) (not(locationDA ?diamond ?agent))
      )
  )

)
