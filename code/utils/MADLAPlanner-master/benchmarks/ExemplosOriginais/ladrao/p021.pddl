;;Problema com dois agentes - eh possivel
(
	define (problem p021) 
	(:domain Ladrao)
	(:objects
		room1 room2 - room
		agent1 - agent
		diamond1 - diamond
		door1 - door
		switch1 - switch
	)
	(:init
		(locationA agent1 room1)
		(locationDR diamond1 room1)
		;;(doorLocked door1)
		(locationS switch1 room2)

	)

	(:goal 
		(and
			(locationDR diamond1 room2)
		)
	)
)
