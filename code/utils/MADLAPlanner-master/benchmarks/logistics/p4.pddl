(define
	(problem tmpProblem)
	(:domain tmpDomain)
	(:objects
		truck2 truck1 truck3 - truck
		cit1 cit2 cit3 - city
		obj11 obj22 obj31 obj23 obj21 obj13 obj12 obj33 obj32 - package
		pos1 apt1 pos2 apt3 pos3 apt2 - location
		plane1 - plane
	)
	(:init
		(isAirport apt1)
		(atTruck truck2 pos2)
		(atTruck truck3 pos3)
		(inCity pos2 cit2)
		(atPackage obj21 pos2)
		(atPackage obj31 pos3)
		(atPackage obj23 pos2)
		(inCity apt3 cit3)
		(atPackage obj33 pos3)
		(isAirport apt3)
		(inCity apt1 cit1)
		(atPackage obj12 pos1)
		(inCity pos1 cit1)
		(atPlane plane1 apt1)
		(atPackage obj32 pos3)
		(inCity apt2 cit2)
		(atPackage obj11 pos1)
		(inCity pos3 cit3)
		(isAirport apt2)
		(atPackage obj13 pos1)
		(atPackage obj22 pos2)
		(atTruck truck1 pos1)
	)
	(:goal
		(and
			(atPackage obj21 pos2)
			(atPackage obj22 pos3)
			(atPackage obj23 apt2)
			(atPackage obj12 pos1)
			(atPackage obj32 apt1)
			(atPackage obj11 pos3)
			(atPackage obj13 apt2)
			(atPackage obj31 apt3)
		)
	)
)