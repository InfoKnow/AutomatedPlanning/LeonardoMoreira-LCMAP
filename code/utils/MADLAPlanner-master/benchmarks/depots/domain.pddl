(define 
  (domain depot)
  (:requirements :typing)
  (:types place hoist surface depot distributor truck crate pallet)
  (:predicates
    (myAgent ?a - place)
    (clearC ?x - crate)
    (clearP ?x - pallet)
    (clearH ?x - hoist)
    (located ?h - hoist ?p - place)
    (at ?t - truck ?p - place)
    (placed ?p - pallet ?p - place)
    (posP ?c - crate ?p - place)
    (posT ?c - crate ?t - truck)
    (onP ?c - crate ?s - pallet)
    (onC ?c - crate ?s - crate)
    (onH ?c - crate ?h - hoist)
    (onT ?c - crate ?t - truck)
    (myAgentT ?a - truck)
  )
 
  (:action LiftP
   :parameters (?h - hoist ?c - crate ?z - pallet ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      (placed ?z ?p) 
      (clearH ?h) 
      (posP ?c ?p)
      (onP ?c ?z) 
      (clearC ?c)
    )
   :effect 
    (and 
      (onH ?c ?h) 
      (not (onP ?c ?z))
      (not (clearC ?c)) 
      (not (clearH ?h))
      (clearP ?z)
    )
  )
  
  (:action LiftC
   :parameters (?h - hoist ?c - crate ?z - crate ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      (posP ?z ?p) 
      (clearH ?h)
      (posP ?c ?p) 
      (onC ?c ?z) 
      (clearC ?c)
    )
   :effect 
    (and 
      (onH ?c ?h)
      (not (onC ?c ?z))
      (not (clearC ?c)) 
      (not (clearH ?h))
      (clearC ?z)
    )
  )
  
  (:action DropP
   :parameters (?h - hoist ?c - crate ?z - pallet ?p - place)
   :precondition 
    (and (myAgent ?p) 
    (located ?h ?p)
    (placed ?z ?p) 
    (clearP ?z) 
    (onH ?c ?h)
    (not (clearC ?c)) 
    (not (clearH ?h)))
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      (not (clearP ?z))
      (onP ?c ?z)
      (not (onH ?c ?h))
    )
  )
  
  (:action DropC
   :parameters (?h - hoist ?c - crate ?z - crate ?p - place)
   :precondition 
    (and 
      (myAgent ?p) 
      (located ?h ?p)
      (posP ?z ?p) 
      (clearC ?z)
      (onH ?c ?h) 
      (not (clearC ?c))
      (not (clearH ?h))
    )
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      (not (clearC ?z))
      (onC ?c ?z)
      (not (onH ?c ?h))
    )
  )
  
(:action Drive
   :parameters (?t - truck ?x - place ?y - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      (at ?t ?x)
    )
   :effect 
    (and 
      (at ?t ?y)
      (not (at ?t ?x))
    )
  )
  
  (:action Load
   :parameters (?h - hoist ?c - crate ?t - truck ?p - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      (at ?t ?p) 
      (posP ?c ?p)
      (not (clearC ?c)) 
      (not (clearH ?h))
      (onH ?c ?h) 
      (located ?h ?p)
    )
   :effect 
    (and 
      (clearH ?h) 
      (clearC ?c) 
      (posT ?c ?t)
      (not (posP ?c ?p))
      (onT ?c ?t)
      (not (onH ?c ?h))
    )
  )
  
  (:action Unload
   :parameters (?h - hoist ?c - crate ?t - truck ?p - place)
   :precondition 
    (and 
      (myAgentT ?t) 
      (located ?h ?p) 
      (at ?t ?p)
      (posT ?c ?t) 
      (onT ?c ?t) 
      (clearH ?h)
      (clearC ?c))
   :effect 
    (and 
      (posP ?c ?p) 
      (not (posT ?c ?t))
      (onH ?c ?h)
      (not (onT ?c ?t))
      (not (clearC ?c)) 
      (not (clearH ?h))
    )
  )

)
