(define 
	(problem ZTRAVEL-2-4)
	(:domain zeno-travel)
	(:objects
		 plane1 plane2 - aircraft
		 person1 person2 person3 person4 - person
		 city0 city1 city2 - city
		 fl0 fl1 fl2 fl3 fl4 fl5 fl6 - flevel
	)
	
	(:init
	 (myAgent plane1)
	 (at plane1 city0)
	 (fuel-level plane1 fl4)
	 (inC person1 city0)
	 (inC person2 city0)
	 (inC person3 city1)
	 (inC person4 city1)
	 (next fl0 fl1)
	 (next fl1 fl2)
	 (next fl2 fl3)
	 (next fl3 fl4)
	 (next fl4 fl5)
	 (next fl5 fl6)
     (myAgent plane2)
	 (at plane2 city2)
	 (fuel-level plane2 fl5)
	)
	(:goal 
		(and
         (at plane2 city2)
		 (inC person1 city1)
		 (inC person2 city0)
		 (inC person3 city0)
		 (inC person4 city1)
		)
	)
)
