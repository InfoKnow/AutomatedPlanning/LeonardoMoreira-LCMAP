(define 
  (domain zeno-travel)
  (:requirements :typing)
  (:types aircraft person city flevel)
  (:predicates
    (myAgent ?a - aircraft)
    (at ?a - aircraft ?c - city)
    (inC ?p - person ?c - city)
    (inA ?p - person ?a - aircraft)
    (fuel-level ?a - aircraft ?l - flevel)
    (next ?l1 - flevel  ?l2 - flevel)
  )
    
  (:action board
    :parameters (?p - person ?a - aircraft ?c - city)
    :precondition 
      (and 
        (myAgent ?a) 
        (at ?a ?c) 
        (inC ?p ?c)
      )
    :effect 
      (and
        (inA ?p ?a)
        (not (inC ?p ?c))
      )
  )

  (:action debark
    :parameters (?p - person ?a - aircraft ?c - city)
    :precondition 
      (and 
        (myAgent ?a) 
        (at ?a ?c) 
        (inA ?p ?a)
      )
    :effect 
      (and
        (inC ?p ?c)
        (not (inA ?p ?a))
      )
  )

  (:action fly
    :parameters (?a - aircraft ?c1 ?c2 - city ?l1 ?l2 - flevel)
    :precondition 
      (and 
        (myAgent ?a) 
        (at ?a ?c1)
        (fuel-level ?a ?l1) 
        (next ?l2 ?l1)
      )
    :effect 
      (and 
        (at ?a ?c2) 
        (not (at ?a ?c1))
        (fuel-level ?a ?l2)
        (not (fuel-level ?a ?l1))
      )
  )
  
  (:action zoom
    :parameters (?a - aircraft ?c1 ?c2 - city ?l1 ?l2 ?l3 - flevel)
    :precondition 
      (and 
        (myAgent ?a) 
        (at ?a ?c1)
        (fuel-level ?a ?l1) 
        (next ?l2 ?l1) 
        (next ?l3 ?l2)
      )
    :effect 
      (and 
        (at ?a ?c2) 
        (not (at ?a ?c1) )
        (fuel-level ?a ?l3)
        (not (fuel-level ?a ?l1))
      )
  )
  
  (:action refuel
    :parameters (?a - aircraft ?c - city ?l - flevel ?l1 - flevel)
    :precondition 
      (and 
        (myAgent ?a) 
        (fuel-level ?a ?l)
        (at ?a ?c) 
        (next ?l ?l1)
      )
    :effect 
      (and
        (fuel-level ?a ?l1)
        (not (fuel-level ?a ?l))
      )
  )
)
