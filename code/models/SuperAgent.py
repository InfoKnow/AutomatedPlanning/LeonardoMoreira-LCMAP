from itertools import product
import pddlpy

class SuperAgent(object):
    
    def __init__(self,parents):

        self.agId = "superAgent"
        self.gops = {}
        self.parents = parents
        for p in parents:
            print('super agent',p.agId)
        self.parser = parents[0].parser

        self.analyseDomainProblem()


    def analyseDomainProblem(self):
        """
        Parseia as seguintes informações do agente:
        1- InitialState
        2- Goals
        3- Actions (as templates)
        """
        
        self.actions = set()
        self.positivePreconditionsByAction = {}
        self.negativePreconditionsByAction = {}
        self.positiveEffectsByAction = {}
        self.negativeEffectsByAction = {}
        self.requiredPredicatesByAction = {}
        #self.operators = {}
        #self.objects = self.parser.getObjects()
        self.objects = set()
        self.configurations = {}
        for ag in self.parents:
            
            for v in ag.objects:
                self.objects.add(v)

            for k,v in ag.gops.items():
                self.gops[k]=v
            
            for a in ag.actions:
                self.actions.add(a)

                if (a not in self.positivePreconditionsByAction):
                    self.positivePreconditionsByAction[a] = set()
                if (a not in self.negativePreconditionsByAction):
                    self.negativePreconditionsByAction[a] = set()
                if (a not in self.positiveEffectsByAction):
                    self.positiveEffectsByAction[a] = set()
                if (a not in self.negativeEffectsByAction):
                    self.negativeEffectsByAction[a] = set()
                if (a not in self.requiredPredicatesByAction):
                    self.requiredPredicatesByAction[a] = set()


                self.positivePreconditionsByAction[a] = self.positivePreconditionsByAction[a].union(ag.positivePreconditionsByAction[a])
                self.negativePreconditionsByAction[a] = self.negativePreconditionsByAction[a].union(ag.negativePreconditionsByAction[a])
                self.positiveEffectsByAction[a] = self.positiveEffectsByAction[a].union(ag.positiveEffectsByAction[a])
                self.negativeEffectsByAction[a] = self.negativeEffectsByAction[a].union(ag.negativeEffectsByAction[a])
                self.requiredPredicatesByAction[a] = self.requiredPredicatesByAction[a].union(ag.requiredPredicatesByAction[a])
            
                if(a not in self.configurations):
                    self.configurations[a] = []
                self.configurations[a].append(ag.configurations[a])

    def getKey(self,item):

        return(item.predicate)

    def groundAction(self,action,configuracao,indiceParametro):

        positivePreconditions = set()                       
        for pc in self.positivePreconditionsByAction[action]: 
            precondition = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                precondition.append(configuracao[indiceParametro.index(p)])
            positivePreconditions.add(str(pddlpy.pddl.Atom(precondition)))

        negativePreconditions = set()
        for pc in self.negativePreconditionsByAction[action]: 
            precondition = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                precondition.append(configuracao[indiceParametro.index(p)])
            negativePreconditions.add(str(pddlpy.pddl.Atom(precondition)))

        positiveEffects = set()
        for pc in self.positiveEffectsByAction[action]: 
            effects = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                effects.append(configuracao[indiceParametro.index(p)])
            positiveEffects.add(pddlpy.pddl.Atom(effects))

        negativeEffects = set()
        for pc in self.negativeEffectsByAction[action]: 
            effects = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                effects.append(configuracao[indiceParametro.index(p)])
            negativeEffects.add(str(pddlpy.pddl.Atom(effects)))

        gop = pddlpy.pddl.Operator(action)
        gop.variable_list = configuracao
        gop.precondition_pos = positivePreconditions.copy()
        gop.precondition_neg = negativePreconditions.copy()
        gop.effect_pos = positiveEffects.copy()
        gop.effect_neg = negativeEffects.copy()
        
        return gop


    def applyAction(self,state,action,setStrState):
        """
        retorna um conjunto de estados depois de aplicar ACTION em STATE
        """

        setNewStates = [] # uso list para permitir adicionar um set

        for conf in self.configurations[action]:

            parametros = conf[0]
            indiceParametro = conf[1]

            #parametros = self.configurations[action][0]
            #indiceParametro = self.configurations[action][1]

            for configuracao in product(*parametros):

                #rotulo = self.agId + action + str(configuracao)
                rotulo =  action + str(configuracao)

                if (rotulo not in self.gops):
                    self.gops[rotulo] = self.groundAction(action,configuracao,indiceParametro)

                if(self.gops[rotulo].precondition_pos.issubset(setStrState) and len(self.gops[rotulo].precondition_neg.intersection(setStrState)) == 0):
                    newState = state.copy()

                    newState = newState.union(self.gops[rotulo].effect_pos)
                    toBeRemoved = set()
                    for p in newState:
                        if(str(p)in self.gops[rotulo].effect_neg):
                            toBeRemoved.add(p)
                    newState = newState.difference(toBeRemoved)
        
                    setNewStates.append((self.gops[rotulo],newState))

        return setNewStates

    def satisfies(self, state, goal):

        strState = set()
        for s in state:
            strState.add(str(s))

        strGoal = set()
        for s in goal:
            strGoal.add(str(s))

        return strGoal.issubset(strState)

    def proximity(self, source, destination):

        strSource = set()
        for s in source:
            strSource.add(str(s))

        strDestination = set()
        for s in destination:
            strDestination.add(str(s))

        return len(strSource.intersection(strDestination))

    def plan(self,initialState, goal):

        target = len(goal)
        matrixFronteira = {}
        visitados = set()
        score = self.proximity(initialState,goal)
        treeLevels = [score]
        currentPlan = None

        plano = ([],initialState)

        matrixFronteira[score] = []
        
        matrixFronteira[score].append(plano)

        while(len(matrixFronteira[score])>0):
            try:
                currentPlan = matrixFronteira[score].pop(0)
                #while(str(sorted(currentPlan[1],key=self.getKey)) in visitados or self.proximity(currentPlan[1],goal)<score):
                while(str(sorted(currentPlan[1],key=self.getKey)) in visitados):
                    try:
                        currentPlan = matrixFronteira[score].pop(0)
                    except IndexError as e:
                        # retorna ao nivel superiror da arvore
                        if(treeLevels.index(score)>0):
                            score = treeLevels[score-1]
                        else:
                            return None

                visitados.add(str(sorted(currentPlan[1],key=self.getKey)))
                currentPlanPredicates = self.parser.getPredicatesInState(currentPlan[1])
                
                setStrState = set()
                for s in currentPlan[1]:
                    setStrState.add(str(s))

                for a in self.actions:
                    if(self.requiredPredicatesByAction[a].issubset(currentPlanPredicates)):
                        for newState in self.applyAction(currentPlan[1],a,setStrState):
                            strNewState = str(sorted(newState[1],key=self.getKey))
                            if(strNewState not in visitados):
                                newScore = self.proximity(newState[1],goal)
                                
                                newPlan = list(currentPlan[0])
                                newPlan.append(newState[0])
                                
                                if(newScore == target):
                                    return newPlan
                                
                                if(newScore not in matrixFronteira):
                                    treeLevels.append(newScore)
                                    score = newScore
                                    matrixFronteira[score] = []
                                matrixFronteira[newScore].append((newPlan,newState[1]))

            except IndexError as e:
                pass       
        #No plan
        return None

    def getPlan(self,initialState,goal):

        return self.plan(initialState,goal)
