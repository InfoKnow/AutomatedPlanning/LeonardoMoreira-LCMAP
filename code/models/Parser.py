import pddlpy

class Parser(object):
   
    def __init__(self,domainPath, problemPath):
        self.pddlObj = pddlpy.DomainProblem(domainPath,problemPath)

    def getInitialState(self):
        return self.pddlObj.initialstate()

    def getGoal(self):
        return self.pddlObj.goals()

    def getActions(self):
        return self.pddlObj.operators()

    def getActionParameters(self, action):

        return self.pddlObj.domain.operators[action].variable_list

    def getActionInfo(self,action,info):
        requiredInfo = set()
        for pc in getattr(self.pddlObj.domain.operators[action],info):
            requiredInfo.add(pc)

        return requiredInfo

    def getPositivePreconditions(self,action):
        return self.getActionInfo(action,'precondition_pos')        

    def getNegativePreconditions(self,action):
        return self.getActionInfo(action,'precondition_neg')

    def getPositiveEffects(self,action):
        return self.getActionInfo(action,'effect_pos')        

    def getNegativeEffects(self,action):
        return self.getActionInfo(action,'effect_neg')        

    def getPredicates(self):
        dictPredicates = {}
        for p in self.getDomain().predicates:
            dictPredicates[p.name]=p
        return dictPredicates

    def getObjects(self):
        #dictObjects[variavel] = tipo
        return self.pddlObj.worldobjects()

    def getTypesAndObjects(self):

        dictTypes = {}
        for v, t in self.getObjects().items():
            if (t not in dictTypes):
                dictTypes[t]=set()
            dictTypes[t].add(v)
        return dictTypes

    def getTypesInState(self,state):
        usedTypes = {}
        dictObjectTypes = self.getObjects()
        for p in state:
            for v in p.predicate[1:]:
                if (dictObjectTypes[v] not in usedTypes):
                    usedTypes[dictObjectTypes[v]] = set()
                usedTypes[dictObjectTypes[v]].add(v)
        return usedTypes

    def getObjectsInState(self,state,rigidPredicates=set()):
        usedObjects = {}
        dictObjectTypes = self.getObjects()
        for p in state:
            if(p.predicate[0] not in rigidPredicates):
                for v in p.predicate[1:]:
                    if (dictObjectTypes[v] not in usedObjects):
                        usedObjects[v] = dictObjectTypes[v]
        return usedObjects

    def getRigidPredicates(self):

        preconditions = set()
        effects = set()

        for action in self.getActions():
            for p in self.getPositivePreconditions(action):
                preconditions.add(p.predicate[0])
            for p in self.getNegativePreconditions(action):
                preconditions.add(p.predicate[0])

            for e in self.getPositiveEffects(action):
                effects.add(e.predicate[0])
            for e in self.getNegativeEffects(action):
                effects.add(e.predicate[0])

        return preconditions.difference(effects)

    def getRequiredPredicatesByAction(self,action):
        predicates = set()
        for p in self.getPositivePreconditions(action):
            predicates.add(p.predicate[0])

        return predicates


    def getPredicatesInState(self, state):

        predicates = set()
        for p in state:
            predicates.add(p.predicate[0])

        return predicates

    def getConfigurations(self, usedTypes = None):

        dictConfigurationsByAction = {}
        if(usedTypes == None):
            usedTypes = self.getTypesAndObjects()

        for action in self.getActions():
            parametros = []
            indiceParametro = []
            for variavel,tipo in self.getActionParameters(action).items():
                if(tipo in usedTypes):
                    parametros.append(list(usedTypes[tipo]))
                    indiceParametro.append(variavel)
            dictConfigurationsByAction[action] = (parametros,indiceParametro)
        return dictConfigurationsByAction

    def getGoalObjects(self):

        goal = self.getGoal()
        goalObjects = set()
        for g in goal:
            for p in g.predicate[1:]:
                goalObjects.add(p)
        return goalObjects