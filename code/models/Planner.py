import subprocess
class Planner(object):
    
    def __init__(self, path, command):

        self.path = path
        self.command = command


    def callPlanner(self,domain,problem):

        #command_line = './utils/FF-v2.3/ff -o %s -f %s'%(domain, problem)
        command_line = self.path + self.command%(domain, problem)
        try:
            output = subprocess.check_output(command_line, shell=True, stderr=subprocess.STDOUT)
            planBeginIndex = str(output).index('step')+5
            planEndIndex = str(output).index('time spent:',planBeginIndex)
            agentPlan = str(output)[planBeginIndex:planEndIndex-4]
            agentPlan = agentPlan.replace('\\n','\n')
            plano = []
            for gop in agentPlan.split('\n'):
                if len(gop)>5:
                    index = gop.index(':')
                    plano.append(gop[index+2:].split(' '))
            
            return plano
        except Exception as e:
            return None
        

    def toGops(self,plano,agentes,dictParameters):

        if(plano != None):
            convertedGops = []
            for gop in plano:
                converted = False
                for ag in agentes:
                    if(not converted):
                        for action in ag.actions:
                            if(action.upper() == gop[0]):
                                converted = True
                                convertedGops.append(ag.groundAction(action,gop[1:],list(dictParameters[action].keys())))
                                break
            return convertedGops
        else:
            return None
