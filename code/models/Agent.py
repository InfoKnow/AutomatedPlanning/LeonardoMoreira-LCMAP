from itertools import product
from models.Parser import *
import pddlpy
import re
from models.Planner import *
import os

class Agent(object):
    
    def __init__(self, agId,domainPath, problemPath):

        
        #print("Instanciando agente",agId)
        self.agId = agId
        self.domainPath = domainPath
        self.problemPath = problemPath
        self.parser = Parser(domainPath,problemPath)
        self.gops = {}
        self.possibleGoals = set() # goals que consegue atingir sozinho
        self.analyseDomainProblem()
        self.rigidPredicates = self.parser.getRigidPredicates()
        #self.planner = Planner('./utils/FF-v2.3/','ff -o %s -f %s')
        
    def analyseDomainProblem(self):
        """
        Parseia as seguintes informações do agente:
        1- InitialState
        2- Goals
        3- Actions (as templates)
        """
        self.initialState = self.parser.getInitialState()
        self.goal = self.parser.getGoal()
        self.actions = self.parser.getActions()
        self.positivePreconditionsByAction = {}
        self.negativePreconditionsByAction = {}
        self.positiveEffectsByAction = {}
        self.negativeEffectsByAction = {}
        self.requiredPredicatesByAction = {}
        self.operators = {}
        
        self.dictParameters = {}
        self.objects = set()
        for v, t in self.parser.getObjects().items():
            self.objects.add((v,t))
        for a in self.actions:
            self.positivePreconditionsByAction[a] = self.parser.getPositivePreconditions(a)
            self.negativePreconditionsByAction[a] = self.parser.getNegativePreconditions(a)
            self.positiveEffectsByAction[a] = self.parser.getPositiveEffects(a)
            self.negativeEffectsByAction[a] = self.parser.getNegativeEffects(a)
            self.requiredPredicatesByAction[a] = self.parser.getRequiredPredicatesByAction(a)
            if a not in self.dictParameters:
                self.dictParameters[a] = self.parser.getActionParameters(a)
            
        self.configurations = self.parser.getConfigurations()
        self.checkGoals(self.initialState,self.goal)

            
    def getKey(self,item):

        return(item.predicate)

    def groundAction(self,action,configuracao,indiceParametro):

        positivePreconditions = set()                       
        for pc in self.positivePreconditionsByAction[action]: 
            precondition = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                precondition.append(configuracao[indiceParametro.index(p)])
            positivePreconditions.add(str(pddlpy.pddl.Atom(precondition)))

        negativePreconditions = set()
        for pc in self.negativePreconditionsByAction[action]: 
            precondition = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                precondition.append(configuracao[indiceParametro.index(p)])
            negativePreconditions.add(str(pddlpy.pddl.Atom(precondition)))

        positiveEffects = set()
        for pc in self.positiveEffectsByAction[action]: 
            effects = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                effects.append(configuracao[indiceParametro.index(p)])
            positiveEffects.add(pddlpy.pddl.Atom(effects))

        negativeEffects = set()
        for pc in self.negativeEffectsByAction[action]: 
            effects = [pc.predicate[0]]
            for p in pc.predicate[1:]:
                effects.append(configuracao[indiceParametro.index(p)])
            negativeEffects.add(str(pddlpy.pddl.Atom(effects)))

        gop = pddlpy.pddl.Operator(action)
        gop.variable_list = configuracao
        gop.precondition_pos = positivePreconditions.copy()
        gop.precondition_neg = negativePreconditions.copy()
        gop.effect_pos = positiveEffects.copy()
        gop.effect_neg = negativeEffects.copy()
        
        return gop


    def applyAction(self,state,action,setStrState):
        """
        retorna um conjunto de estados depois de aplicar ACTION em STATE
        """

        setNewStates = [] # uso list para permitir adicionar um set

        parametros = self.configurations[action][0]
        indiceParametro = self.configurations[action][1]

        for configuracao in product(*parametros):

            #rotulo = self.agId +'='+ action + str(configuracao)
            rotulo = action + str(configuracao)

            if (rotulo not in self.gops):
                self.gops[rotulo] = self.groundAction(action,configuracao,indiceParametro)

            if(self.gops[rotulo].precondition_pos.issubset(setStrState) and len(self.gops[rotulo].precondition_neg.intersection(setStrState)) == 0):
                newState = state.copy()
                newState = newState.union(self.gops[rotulo].effect_pos)

                toBeRemoved = set()
                for p in newState:
                    if(str(p)in self.gops[rotulo].effect_neg):
                        toBeRemoved.add(p)
                newState = newState.difference(toBeRemoved)
                
                setNewStates.append((self.gops[rotulo],newState))

        return setNewStates

    def satisfies(self, state, goal):

        strState = set()
        for s in state:
            strState.add(str(s))

        strGoal = set()
        for s in goal:
            strGoal.add(str(s))

        return strGoal.issubset(strState)

    def plan(self,initialState, goal):

        if (not self.satisfies(self.initialState,goal)):
            # só planeja se o objetivo nao estiver satisfeito

            dictObjects = {}
            for obj in self.objects:
                if obj[1] not in dictObjects:
                    dictObjects[obj[1]] = set()
                dictObjects[obj[1]].add(obj[0])
            
            #############################Domain File ######################################
            #regenerating domain file
            fileDomain = self.agId +"_tmpDomain.pddl"
            file = open(fileDomain,"w")

            #opening
            file.write('(define\n')
            file.write('\t(domain tmpDomain)\n')
            file.write('\t(:requirements :typing)\n')
            file.write('\t(:types')
            
            #types
            for tipo in dictObjects.keys():
                file.write(' '+tipo)
            file.write(')\n')

            #predicates

            file.write('\t(:predicates')


            setPredicates = set()
            setPredicateNames = set()
            dictParameters = {}
            dictPrePos = {}
            dictPreNeg = {}
            dictEffPos = {}
            dictEffNeg = {}
            
            for action in self.actions:
                parameters = self.parser.getActionParameters(action)
                #verificando se alguma acao tem algum parametro sem algum objeto do mesmo tipo
                # se tiver...crio um virtual
                for parameterType in parameters.values():
                    if parameterType not in dictObjects:
                        dictObjects[parameterType] = 'virtual'+parameterType

                if(action not in dictParameters):
                    dictParameters[action] = parameters
                dictPrePos[action] = self.positivePreconditionsByAction[action]
                for p in self.positivePreconditionsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictPreNeg[action] = self.negativePreconditionsByAction[action]
                for p in self.negativePreconditionsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictEffPos[action] = self.positiveEffectsByAction[action]

                for p in self.positiveEffectsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictEffNeg[action] = self.negativeEffectsByAction[action]

                for p in self.negativeEffectsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])
    
            for p in setPredicates:
                file.write('\n\t\t'+p)

            #print(setPredicateNames)

            
            if('(water ?watersqr - square)') not in setPredicates:
                file.write('\n\t\t(water ?watersqr - square)')
            
            if('(fort ?fortsqr - square)') not in setPredicates:
                file.write('\n\t\t(fort ?fortsqr - square)')
            

            file.write('\n\t)')

            #actions
            for action in self.actions:
                file.write('\n\t(:action '+str(action)+'\n')
                #parameters
                file.write('\t\t:parameters (')
                pars1 =re.sub('[\',\{\}]','',str(dictParameters[action]))
                pars2 =re.sub(':',' -',pars1)
                file.write(pars2+')')
                
                #preconditions
                file.write('\n\t\t:precondition\n')
                file.write('\t\t\t(and\n')
                if(len(dictPrePos[action])>0):
                    prePos =re.sub('[\',\{\}]','',str(dictPrePos[action]))
                    file.write('\t\t\t\t'+prePos)

                if(len(dictPreNeg[action])>0):
                    preNeg1 =re.sub('[\',\{\}]','',str(dictPreNeg[action]))
                    preNeg2 =re.sub('\(','(not(',preNeg1)
                    preNeg3 =re.sub('\)','))',preNeg2)
                    file.write('\n\t\t\t\t'+preNeg3)

                file.write('\n\t\t\t)\n')

                #effects
                file.write('\n\t\t:effect\n')
                file.write('\t\t\t(and\n')

                if(len(dictEffPos[action])>0):
                    effPos =re.sub('[\',\{\}]','',str(dictEffPos[action]))
                    file.write('\t\t\t\t'+effPos)

                if(len(dictEffNeg[action])>0):
                    effNeg1 =re.sub('[\',\{\}]','',str(dictEffNeg[action]))
                    effNeg2 =re.sub('\(','(not(',effNeg1)
                    effNeg3 =re.sub('\)','))',effNeg2)
                    file.write('\n\t\t\t\t'+effNeg3)

                file.write('\n\t\t\t)')
                file.write('\n\t)\n')

            file.write(')\n')
            file.close()

            #############################Problem File ######################################
            #regenerating problem file 
            fileProblem = self.agId +"_tmpProblem.pddl"
            file = open(fileProblem,"w")

            #opening
            file.write('(define\n')
            file.write('\t(problem tmpProblem)\n')
            file.write('\t(:domain tmpDomain)\n')
            file.write('\t(:objects\n')

            for tipo, obj in dictObjects.items():
                o = re.sub('[\',\{\}]','',str(obj))
                file.write('\t\t'+o+' - '+str(tipo)+'\n')

            file.write('\t)\n')


            #initialState

            file.write('\t(:init\n')
            #file.write('\t\t(fort sq14)\n')
            
            for initState in self.initialState:
                i = re.sub('[\',]','',str(initState))
                file.write('\t\t'+i+'\n')

            file.write('\t)\n')

            #goals
            file.write('\t(:goal\n\t\t(and\n')

            for g in goal:
                t = re.sub('[\',]','',str(g))
                file.write('\t\t\t'+t+'\n')

            file.write('\t\t)\n\t)\n')

            #closing
            file.write(')')
            file.close()
            self.planner = Planner('./utils/FF-v2.3/','ff -o %s -f %s')
            plano =  self.planner.toGops(self.planner.callPlanner(fileDomain,fileProblem),[self],dictParameters)
            #os.remove(fileDomain)
            #os.remove(fileProblem)
            #print(plano)
            return plano
            #return None
        else:
            #return "Objetivo já satisfeito. Nenhuma ação necessária!"
            return []


    
    def HFF(self,state, goal):

        """
        Calcula a heurística HFF do Ghalab ag 53.
        Na verdade estou usando pra verificar se o goal é possível
        """

        hatS = []
        hatS.append(state)      

        rApplicableActions = [] 
        rApplicableActions.append(None)
        k=1
        while(not self.satisfies(hatS[k-1],goal)):
            currentStatePredicates = self.parser.getPredicatesInState(hatS[k-1])
            
            newState = hatS[k-1].copy()
            setStrState = set()
            for s in hatS[k-1]:
                setStrState.add(str(s))

            for action in self.actions:
                if(self.requiredPredicatesByAction[action].issubset(currentStatePredicates)):

                    parametros = self.configurations[action][0]
                    indiceParametro = self.configurations[action][1]

                    for configuracao in product(*parametros):
                        #rotulo = self.agId +'=' + action + str(configuracao)
                        rotulo = action + str(configuracao)
                        
                        if (rotulo not in self.gops):
                            self.gops[rotulo] = self.groundAction(action,configuracao,indiceParametro)

                        if(self.gops[rotulo].precondition_pos.issubset(setStrState) and len(self.gops[rotulo].precondition_neg.intersection(setStrState)) == 0):
                            newState = newState.union(self.gops[rotulo].effect_pos)

            hatS.append(newState)

            setK0 = set()
            setK1 = set()

            for p in hatS[k]:
                setK0.add(str(p))

            for p in hatS[k-1]:
                setK1.add(str(p))

            if(setK0 == setK1):
                return False
            else:
                k = k +1
        return True        

    def checkGoals(self,state, goal):

        if(self.HFF(state,goal)):
            #self.possibleGoals = self.goal.copy()
            self.possibleGoals = self.goal

        else:
            for g in goal:
                tmpGoal = set()
                tmpGoal.add(g)
                if(self.HFF(state,tmpGoal)):
                    self.possibleGoals.add(g)

    def getPlan(self,goal):

        return self.plan(self.initialState,goal)
